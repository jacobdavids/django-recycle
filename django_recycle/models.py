import uuid

from django.db import models


class HasName(models.Model):
    name = models.CharField(max_length=100, blank=True)

    class Meta:
        abstract = True


class HasUniqueName(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        abstract = True


class HasDescription(models.Model):
    description = models.TextField(blank=True)

    class Meta:
        abstract = True


class HasUUID(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4)

    class Meta:
        abstract = True


class HasExtraData(models.Model):
    extra_data = models.JSONField(default=dict, blank=True)

    class Meta:
        abstract = True


class HasNotes(models.Model):
    notes = models.TextField(blank=True)

    class Meta:
        abstract = True


class HasTimestamp(models.Model):
    timestamp = models.DateTimeField()

    class Meta:
        abstract = True


class TracksCreatedAndUpdated(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
